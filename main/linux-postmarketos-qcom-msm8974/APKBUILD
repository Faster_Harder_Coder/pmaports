# Maintainer: Luca Weiss <luca@z3ntu.xyz>
_flavor=postmarketos-qcom-msm8974
_config="config-$_flavor.$CARCH"

pkgname=linux-$_flavor
pkgver=5.6.0_rc6
pkgrel=7
_commit="b9f39bdf61e5c8f5db63afe7ab1c9ff77aa6b4bc"
pkgdesc="Kernel close to mainline with extra patches for Qualcomm MSM8974 devices"
arch="armv7"
_carch="arm"
url="https://kernel.org/"
license="GPL-2.0-only"
options="!strip !check !tracedeps pmb:cross-native"
makedepends="
	bison
	findutils
	flex
	installkernel
	openssl-dev
	perl
	"
source="https://gitlab.com/postmarketOS/linux-postmarketos/-/archive/$_commit/linux-postmarketos-$_commit.tar.gz
	config-$_flavor.armv7
	fix-gcc10-build.patch
	"
builddir="$srcdir/linux-postmarketos-$_commit"

prepare() {
	default_prepare
	cp "$srcdir/config-$_flavor.$CARCH" .config
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION=$((pkgrel + 1 ))
}

package() {
	mkdir -p "$pkgdir"/boot
	make zinstall modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_PATH="$pkgdir"/boot \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_DTBS_PATH="$pkgdir"/usr/share/dtb
	rm -f "$pkgdir"/lib/modules/*/build "$pkgdir"/lib/modules/*/source

	install -D "$builddir/include/config/kernel.release" \
		"$pkgdir/usr/share/kernel/$_flavor/kernel.release"
}

sha512sums="92866a037fd1bd3b17572754548c19423f6e762d90fb1ebca52c252ace85c87b6f747515f8606fe627d34c59828cbebf83054b9d6e5dd977e0398d9c46fcc176  linux-postmarketos-b9f39bdf61e5c8f5db63afe7ab1c9ff77aa6b4bc.tar.gz
bcaf8bcba8a16cf6ff0218fe857a2c8e064edf495d168422c80e4785e1b2b17346319a3bcb3f1a33f9f6caab8cb4a13032dfd1cfdba40bb2acb74379c52d077a  config-postmarketos-qcom-msm8974.armv7
fcb8c823e9943f409876bbec88e78ee2bbac3a30888ed3b10eb790157f568167e29d37763af4ebbf3ae9ce7fc0ed7f39710f58487069579a5eafa7e540e1e7e1  fix-gcc10-build.patch"
